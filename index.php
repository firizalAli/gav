<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale = 1.0, user-scalable = no, width=device-width, height=device-height, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- <link type="image/x-icon" rel="shortcut icon" href="images/favicon.ico">
 -->
    <title>Gavinesia</title>

    <!-- Core CSS -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,700" rel="stylesheet">
    <link href="css/chosen.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>
</head>
<body>
<div class="container">
	<div class="content">

		<!-- S:MAP -->
		<div id="map"></div>
		<div style="display:none">
      <div class="controls zoom-control">
        <button class="zoom-control-in" title="Zoom In">+</button>
        <button class="zoom-control-out" title="Zoom Out">−</button>
      </div>
      <div class="controls maptype-control maptype-control-is-map">
        <button class="maptype-control-map"
                title="Show road map">Map</button>
        <button class="maptype-control-satellite"
                title="Show satellite imagery">Satellite</button>
      </div>
      <div class="controls fullscreen-control">
        <button title="Toggle Fullscreen">
          <div class="fullscreen-control-icon fullscreen-control-top-left"></div>
          <div class="fullscreen-control-icon fullscreen-control-top-right"></div>
          <div class="fullscreen-control-icon fullscreen-control-bottom-left"></div>
          <div class="fullscreen-control-icon fullscreen-control-bottom-right"></div>
        </button>
      </div>
    </div>	
    	<!-- MAP CONTROLLER -->
		<script src="js/map-controller.js"></script>
    	<!-- MAP CONTROLLER -->
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzPrIQBwYj2Do80x7F_Y0dAFpRHFH9AFA&callback=initMap" async defer></script>
		<!-- E:MAP -->

		<!-- LOGO -->
		<div class="head-logo-area">
			<a href="#"><h1><img src="images/logo.png" alt="Gavinesia - Map"></h1></a>
		</div>
		<!-- E: LOGO -->

		<!-- S: CONTENT LEFT AREA HIDDEN -->
		<div class="content-area">
			<div class="close-content-area close-left-info"><span>+</span></div>
			<div class="content-area-inside">

				<!-- CONTENT 1 -->
				<div class="content-text-area">
					<span class="ratiobox ratio_16_9 image-top image-top-2">
	                    <span class="ratiobox_content lqd">
	                        <img src="https://www.nusantaratv.com/uploads/1512557812.jpg" alt="bu1">
	                    </span>
	                </span>
	                <center><h3 class="bold">Informasi Kecamatan</h3></center>
					<ul>
						<li><span>01.004.K.1-JP-MT-MTG-16</span></li>
						<li><span>KELURAHAN</span> MENTENG</li>
						<li><span>KECAMATAN</span> TANJUNG PRIOK</li>
						<li><span>KOTA</span> 	JAKARTA UTARA</li>
						<li> <span>KODE_BLOK</span> 	04</li>
						<li> <span>SUB_BLOK</span> 	092</li>
						<li> <span>SUB_ZONA</span> 	R.4</li>
						<li> <span>CD_TPZ</span> </li>
						<li> <span>ID_SUBBLOK</span> 	092.R.4</li>
						<li> <span>ID</span> 	04.092.R.4-JU-TJ-SAG-150</li>
						<li> <span>ZONA</span> 	ZONA PERUMAHAN KDB SEDANG-TINGGI</li>
						<li> <span>SUBZONA</span> 	Sub Zona Rumah Sedang</li>
						<li> <span>LUAS_M2</span> 	367,810.14</li>
						<li> <span>KDB</span> 	60</li>
						<li> <span>KB</span> 	2</li>
						<li> <span>KDH</span> 	20</li>
						<li> <span>KTB</span> 	-</li>
						<li> <span>TIPE</span> 	D</li>
						<li> <span>PSL</span> 	P</li>
						<li>
							<span>DIIJINKAN	</span>

							Penitipan Anak, Musholla, Rumah Dinas, Hunian Taman, Rumah Sedang, Kegiatan Kepentingan Pertahanan, Kolam Retensi, Taman Kota, Hutan Kota, Praktek Bidan, Puskesmas, Reklame, Parkir Kendaraan, Parkir Sepeda, Lapangan Olahraga, Taman Rekreasi, Tempat Berma

						</li>
						<li>
							<span>BERSYARAT</span>	PKL, Vihara, Kelenteng, Pura, Gereja, Masjid, Paviliun, Guest House, Panti Jompo, Panti Asuhan dan Yatim Piatu, Rumah Kos, Asrama, Rumah Susun Umum, Hunian Susun Taman, Rumah Susun, Rumah Flat, Rumah Besar, Rumah Kecil, Rumah Sangat Kecil, Daur Ulang, In

						</li>
						<li>
							<span>TERBATAS</span>	Warung Telekomunikasi, Mini Market, Kantor Lembaga Sosial dan Organisasi Kemasyarakatan, Sanggar Seni, Apotik, Balai Pengobatan, Perkantoran Perwakilan Negara Asing, Perkantoran Pemerintahan Daerah, Perkantoran Pemerintahan Nasional, Penjahit (Tailor), P

						</li>
					</ul>
					<center> <span class="close-inside-area-button">Close</span> </center>

				</div>
				<!-- END CONTENT 1 -->

				<!-- CONTENT 2 -->
                <div class="inside-other">
					<span class="ratiobox ratio_16_9 image-top">
	                    <span class="ratiobox_content lqd">
	                        <img src="https://www.nusantaratv.com/uploads/1512557812.jpg" alt="bu1">
	                    </span>
	                </span>
                	<h2>Potensi Investasi</h2>
                	<span class="cta-title">Nama</span>
                	<span class="cta-info">Stadion-BMW</span>
                	<span class="cta-title">Alamat</span>
                	<span class="cta-info">Kelurahan Papanggo, Kec Tanjung Priok, Jakarta Utara</span>
                	<span class="cta-title">Luas</span>
                	<span class="cta-info">16,9 HA</span>
                	<span class="cta-title">Nilai Investasi</span>
                	<span class="cta-info">$ 45,000.-</span>
                	<span class="cta-title">Dokumentasi</span>
                	<span class="cta-info"><a href="#">Klik Untuk Mengunduh</a></span>
                	<span class="cta-title">RDTR</span>
                	<span class="cta-info mb20">
                		<label class="container">
						  <input type="checkbox">
						  <span class="rdtr-chk"></span>
						  <i>Klik Layer RDTR untuk informasi detail</i>
						</label>
						
                	</span>
                	<span class="cta-title">Transparansi RDTR</span>
                	<input id="rs-range-line" class="rs-range mb20" type="range" value="0" min="0" max="100">
                	<div class="clearfix mt20"></div>
                	<span class="cta-title">Profil Video</span>
                	<div class="clearfix mb20"></div>
                	<span class="ratiobox ratio_4_3 image-top mt20">
	                    <span class="ratiobox_content lqd">
	                        <iframe src="https://www.youtube.com/embed/F5yRxOQBaAo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	                    </span>
	                </span>

					<div class="clearfix mt20"></div>
                	<span class="cta-title">Gallery</span>
					<div class="gallery-area">
						<a data-href="image1.html" class="p_modal_show">
		                	<span class="ratiobox ratio_16_9">
			                    <span class="ratiobox_content lqd">
			                        <img src="https://www.nusantaratv.com/uploads/1512557812.jpg"  alt="bu1">
			                    </span>
			                </span>
		                </a>
		                <a data-href="image2.html" class="p_modal_show">
	                	<span class="ratiobox ratio_16_9">
		                    <span class="ratiobox_content lqd">
		                        <img src="https://idetrips.com/wp-content/uploads/2018/08/masjid-istiqlal-640x417.jpg" alt="bu1">
		                    </span>
		                </span>
		                </a>
		                <a data-href="image1.html" class="p_modal_show">
	                	<span class="ratiobox ratio_16_9">
		                    <span class="ratiobox_content lqd">
		                        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSCgxPuUmpyGUp-mEIWxMGQ02z77sC21bFGWzLqjahIcxf33SgI3Q" alt="bu1">
		                    </span>
		                </span>
		                </a>
		                <a data-href="image2.html" class="p_modal_show">
		                <span class="ratiobox ratio_16_9">
		                    <span class="ratiobox_content lqd">
		                        <img src="https://media-cdn.tripadvisor.com/media/photo-s/07/5b/a0/67/waduk-jatiluhur.jpg" alt="bu1">
		                    </span>
		                </span>
		                </a>
					</div>					
					

                </div>
				<!-- END CONTENT 2 -->

			</div>
		</div>
		<!-- E: CONTENT LEFT AREA HIDDEN -->
		
		<!-- S : SAMPLE CLICK MAP -->
		<span class="button-left-area">Area Click 1</span>
		<span class="button-left-area-other">Area Click 2</span>
		<!-- S : SAMPLE CLICK MAP -->

		<!-- S: FILTER PROPERTY AREA -->
		<div class="filter-area">
			<div class="close-content-area close-filter"><span>+</span></div>
			<h2>Filter</h2>
    		<span class="cta-info mb20">
        		<label class="container">
				  <input type="checkbox">
				  <span class="rdtr-chk"></span>
				  <i>RDTR</i>
				</label>
				
        	</span>

    		<span class="cta-info mb20">
        		<label class="container">
				  <input type="checkbox">
				  <span class="rdtr-chk"></span>
				  <i>iVision</i>
				</label>
				
        	</span>

    		<span class="cta-info mb20">
        		<label class="container">
				  <input type="checkbox">
				  <span class="rdtr-chk"></span>
				  <i>aerial Map</i>
				</label>
				
        	</span>

    		<span class="cta-info mb20">
        		<label class="container">
				  <input type="checkbox">
				  <span class="rdtr-chk"></span>
				  <i>aerial Map 2014</i>
				</label>
				
        	</span>

	    	<span class="cta-title">Transparansi RDTR</span>
	    	<input id="rs-range-line" class="rs-range mb10" type="range" value="0" min="0" max="100">
		</div>

		<!-- E: FILTER PROPERTY AREA -->

		<!-- S: MENU FOOTER AREA -->
		<div class="menu-footer-area">
			<div class="menu-footer-area-left">
				<span class="filter-btn">Filter</span>
				<div id="search-desktop">
					<div class="search-area">
						<input type="text" name="search" placeholder="Search">
						<button value="Search"></button>
					</div>
				</div>
				<span class="tools-btn">Tools</span>
			</div>

			<div class="mobile-menu-area">

				<div class="menu-footer-area-right">
					<div id="search-area-mobile">
						<div class="close-content-area close-tools"><span>+</span></div>
						<h2>Tools</h2>
					</div>
					<select data-placeholder="Pilih Kelurahan ..." class="chosen-select" tabindex="2">
					    <option value=""></option>
					    <option value="Kelurahan">Kelurahan</option>
					    <option value="Kelurahan 1">Kelurahan 1</option>
					    <option value="Kelurahan 2">Kelurahan 2</option>
					    <option value="Kelurahan 3">Kelurahan 3</option>
					    <option value="Kelurahan 4">Kelurahan 4</option>
					    <option value="Kelurahan 5">Kelurahan 5</option>
					    <option value="Kelurahan">Kelurahan</option>
					    <option value="Kelurahan 1">Kelurahan 6</option>
					    <option value="Kelurahan 2">Kelurahan 7</option>
					    <option value="Kelurahan 3">Kelurahan 8</option>
					    <option value="Kelurahan 4">Kelurahan 9</option>
					    <option value="Kelurahan 5">Kelurahan 10 Panjang Nama Kelurahan nya</option>
					</select>

					<select data-placeholder="Pilih Kelurahan ..." class="chosen-select" tabindex="2">
					    <option value=""></option>
					    <option value="Kelurahan">Kecamatan</option>
					    <option value="Kecamatan 1">Kecamatan 1</option>
					    <option value="Kecamatan 2">Kecamatan 2</option>
					    <option value="Kecamatan 3">Kecamatan 3</option>
					    <option value="Kecamatan 4">Kecamatan 4</option>
					    <option value="Kecamatan 5">Kecamatan 5</option>
					</select>

					<select data-placeholder="Pilih Potensi Investasi ..." class="chosen-select" tabindex="2">
					    <option value=""></option>
					    <option value="Potensi Investasi">Potensi Investasi</option>
					    <option value="Potensi Investasi 1">Potensi Investasi 1</option>
					    <option value="Potensi Investasi 2">Potensi Investasi 2</option>
					    <option value="Potensi Investasi 3">Potensi Investasi 3</option>
					    <option value="Potensi Investasi 4">Potensi Investasi 4</option>
					    <option value="Potensi Investasi 5">Potensi Investasi 5</option>
					</select>

				</div>
			
			</div>

		</div>
		<!-- E: MENU FOOTER AREA -->
	</div>

</div>
</body>
<script src="js/chosen.jquery.js" type="text/javascript"></script>
<script src="js/jquery-controller.js"></script>
</html>