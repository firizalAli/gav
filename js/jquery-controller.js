
$(document).ready(function(){

// Show Hide Content Slide From Left
$('.button-left-area').click(function()
{
	$(".content-area").animate({left:'0'});
	$(".content-text-area ").hide(300);       
});
$('.button-left-area-other').click(function()
{
	$(".content-area").animate({left:'0'});
	$(".content-text-area ").show(100);       
});
$('.close-left-info').click(function()
{
    $(".content-area").animate({left:'-200%'});
    $(".content-text-area ").hide(300);        
});

$('.close-inside-area-button').click(function()
{
    $(".content-text-area ").hide(300);        
});


$(".filter-btn, .close-filter").click(function () {
   $(".filter-area").toggleClass("active-left");
   $(".mobile-menu-area").removeClass("active-left");
});

$(".tools-btn, .close-tools").click(function () {
   $(".mobile-menu-area").toggleClass("active-left");
   $(".filter-area").removeClass("active-left");
});

// Choosen
//$(".chosen-select").chosen()
$(".chosen-select").chosen({disable_search :true});

// Liquid Image Prop
function liquid($param1) {
    var lqd = $($param1);
    $(lqd).each(function(index){
      var img_src = $(this).find('img').attr( "src" );
      if($param1 == ".lqd"){
        $(this).find('img').hide();
        $(this).css('background-image', 'url(' + img_src + ')');
      }
      else {
        $(this).append('<div class="lqd_img" style="background-image:url('+ img_src +');"></div>');
      }
    });
    return;
  }


liquid('.lqd');
liquid('.lqd_block');

// Slider Prop
var rangeSlider = document.getElementById("rs-range-line");
var rangeBullet = document.getElementById("rs-bullet");

rangeSlider.addEventListener("input", showSliderValue, false);

function showSliderValue() {
  rangeBullet.innerHTML = rangeSlider.value;
  var bulletPosition = (rangeSlider.value /rangeSlider.max);
  rangeBullet.style.left = (bulletPosition * 578) + "px";
}

// MODALBOX 
(function($) {

	$.fn.modalBox = function(options) {

		// default settings
		var settings = $.extend({
			// do value something
		}, options);

		return this.click(function(e) {
			e.preventDefault();

			var btn_modal = $(this);
			var src = btn_modal.attr("data-href");

			setTimeout(function() {
	    		$("body").addClass("modal_open");
	    		$(".modal_box").addClass("anim");
	        },100);
	        $("body").append("<div class='modal_box'><div class='modal_dialog'><div class='modal_content'><a href='#' class='modal_close'>X</a><div class='lds-ring'><div></div><div></div><div></div><div></div></div><div class='modal_body'></div></div></div></div><div class='modal_overlay'></div>");
	        $.ajax({
	        	url:src,
	        	cache:false,
	        	beforeSend: function() {
			      	$(".lds-ring").show();
			   	},
	        	success:function(loadedContent){
	        		$(".modal_content").addClass("loaded")
	            	$(".modal_box").find(".modal_body").html(loadedContent);
	            	$(".lds-ring").hide();
		        }
		    });
		    $(".modal_close").click(function(e) {
		    	e.preventDefault();
		        var this_modal = $(this).parents(".modal_box");
		        setTimeout(function() {
					$(".modal_overlay").remove();
					$(this_modal).remove();
				},300);
				$(".modal_box").removeClass("anim");
		        $("body").removeClass("modal_open");
		    });
		    $(".modal_box").mouseup(function(e) {
		    	var this_modal = $(this);
		        var modal_container = $(".modal_content");
		        if (!modal_container.is(e.target) // if the target of the click isn't the container...
		            && modal_container.has(e.target).length === 0) // ... nor a descendant of the container
		        {
					setTimeout(function() {
						$(".modal_overlay").remove();
						$(this_modal).remove();
					},300);
					$(".modal_box").removeClass("anim");
		            $("body").removeClass("modal_open");
		        }
		    });
		});
	}

}(jQuery));

$(".p_modal_show").modalBox();

//detach
if (matchMedia) {
  var mq = window.matchMedia('(min-width: 767px)');
  var searchArea = $("#search-desktop .search-area");
  
  mq.addListener(WidthChange);
  WidthChange(mq);
}

function WidthChange(mq) {
  if (mq.matches) {
    
    searchArea.appendTo("#search-desktop");
    searchArea = null;


  } else {
    
    searchArea = $("#search-desktop .search-area").detach(); 
    searchArea.appendTo("#search-area-mobile");

  }
};


});




